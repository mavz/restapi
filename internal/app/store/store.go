package store

type Store interface {
	User() UserRepository
	Collection() CollectionRepository
	Tag() TagRepository
	Provaider() ProvaiderRepository
	ProvaiderItem() ProvaiderItemRepository
	CollectionTags() CollectionTagsRepository
	ProvaiderItemTags() ProvaiderItemTagsRepository
	Session() SessionRepository
}
