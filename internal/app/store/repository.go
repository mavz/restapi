package store

import "bitbucket.org/mavz/restapi.git/internal/app/model"

type UserRepository interface {
	Create(*model.User) error
	Find(int) (*model.User, error)
	FindByEmail(string) (*model.User, error)
}

type CollectionRepository interface {
	Create(*model.Collection) error
	Update(*model.Collection) error
	Delete(string) (int, error)
	FindId(string) (*model.Collection, error)
	FindAll() ([]model.Collection, error)
	FindAllPagination(*model.Search) ([]model.Collection, error)
}

type TagRepository interface {
	Create(*model.Tag) error
	Update(*model.Tag) error
	Delete(string) (int, error)
	FindId(string) (*model.Tag, error)
	FindAll() ([]model.Tag, error)
	FindAllPagination(*model.Search) ([]model.Tag, error)
}

type ProvaiderRepository interface {
	Create(*model.Provaider) error
	Update(*model.Provaider) error
	Delete(string) (int, error)
	FindId(string) (*model.Provaider, error)
	FindAll() ([]model.Provaider, error)
	FindAllPagination(*model.Search) ([]model.Provaider, error)
}

type ProvaiderItemRepository interface {
	Create(*model.ProvaiderItem) error
	Update(*model.ProvaiderItem) error
	Delete(string) (int, error)
	FindId(string) (*model.ProvaiderItem, error)
	FindAll(string) ([]model.ProvaiderItem, error)
	FindAllPagination(string, *model.Search) ([]model.ProvaiderItem, error)
	FindAllPaginationwithTags(string, *model.Search) ([]model.ProvaiderItem, error)
}

type CollectionTagsRepository interface {
	AddTag(*model.CollectionTag) error
	DeleteTag(*model.CollectionTag) (int, error)
	FindAllTags(string) ([]model.Tag, error)
	FindAllTagsPagination(string, *model.Search) ([]model.Tag, error)
	FindSelectTags(string, string) ([]model.Tag, error)
}

type ProvaiderItemTagsRepository interface {
	AddTag(*model.ProvaiderItemTag) error
	DeleteTag(*model.ProvaiderItemTag) (int, error)
	FindAllTags(string) ([]model.Tag, error)
	FindAllTagsPagination(string, *model.Search) ([]model.Tag, error)
	FindSelectTags(string, string, string) ([]model.Tag, error)
}

type SessionRepository interface {
	AddSession(*model.Session) error
	DeleteByToken(string) error
	DeleteByUser(int) error
	FindByToket(string) (*model.Session, error)
}
