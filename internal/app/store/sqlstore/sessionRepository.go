package sqlstore

import (
	"database/sql"
	"fmt"

	"bitbucket.org/mavz/restapi.git/internal/app/model"
	"bitbucket.org/mavz/restapi.git/internal/app/store"
)

type SessionRepository struct {
	store *Store
}

func (r *SessionRepository) AddSession(s *model.Session) error {
	if err := s.Validate(); err != nil {
		return err
	}
	tx, err := r.store.db.Begin()
	if err != nil {
		return err
	}
	if _, err := r.store.db.Exec("DELETE FROM session WHERE user_id = $1 OR token = $2",
		s.User,
		s.Token,
	); err != nil {
		tx.Rollback()
		return err
	}
	if _, err := r.store.db.Exec(
		"INSERT INTO session (user_id, token, data) VALUES ($1, $2, $3)",
		s.User,
		s.Token,
		s.Data,
	); err != nil {
		tx.Rollback()
		return err
	}
	if err := tx.Commit(); err != nil {
		return err
	}
	return nil
}

func (r *SessionRepository) DeleteByToken(string) error {
	//TODO подождать, может и не понадобится
	return nil
}

func (r *SessionRepository) DeleteByUser(user_id int) error {

	res, err := r.store.db.Exec("DELETE FROM session WHERE user_id = $1", user_id)
	if err != nil {
		return err
	}
	count, err := res.RowsAffected()
	if err != nil || count == 0 {
		return err
	}
	return nil
}

func (r *SessionRepository) FindByToket(token string) (*model.Session, error) {
	s := &model.Session{}
	fmt.Println("Токен, который мы ищем в таблице", token)
	if err := r.store.db.QueryRow(
		"SELECT user_id, token, data FROM session WHERE token = $1",
		token,
	).Scan(
		&s.User,
		&s.Token,
		&s.Data,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, store.ErrRecordNotFound
		}
		return nil, err
	}
	return s, nil
}
