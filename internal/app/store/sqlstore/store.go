package sqlstore

import (
	"bitbucket.org/mavz/restapi.git/internal/app/store"
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
)

type Store struct {
	db                          *sqlx.DB
	userRepository              *UserRepository
	collectionRepository        *CollectionRepository
	tagRepository               *TagRepository
	provaiderRepository         *ProvaiderRepository
	provaiderItemRepository     *ProvaiderItemRepository
	collectionTagsRepository    *CollectionTagsRepository
	provaiderItemTagsRepository *ProvaiderItemTagsRepository
	sessionRepository           *SessionRepository
}

func New(db *sqlx.DB) *Store {
	return &Store{
		db: db,
	}
}

func (s *Store) User() store.UserRepository {
	if s.userRepository != nil {
		return s.userRepository
	}
	s.userRepository = &UserRepository{
		store: s,
	}
	return s.userRepository
}

func (s *Store) Collection() store.CollectionRepository {
	if s.collectionRepository != nil {
		return s.collectionRepository
	}
	s.collectionRepository = &CollectionRepository{
		store: s,
	}
	return s.collectionRepository
}

func (s *Store) Tag() store.TagRepository {
	if s.tagRepository != nil {
		return s.tagRepository
	}
	s.tagRepository = &TagRepository{
		store: s,
	}
	return s.tagRepository
}

func (s *Store) Provaider() store.ProvaiderRepository {
	if s.provaiderRepository != nil {
		return s.provaiderRepository
	}
	s.provaiderRepository = &ProvaiderRepository{
		store: s,
	}
	return s.provaiderRepository
}

func (s *Store) ProvaiderItem() store.ProvaiderItemRepository {
	if s.provaiderItemRepository != nil {
		return s.provaiderItemRepository
	}
	s.provaiderItemRepository = &ProvaiderItemRepository{
		store: s,
	}
	return s.provaiderItemRepository
}

func (s *Store) CollectionTags() store.CollectionTagsRepository {
	if s.collectionTagsRepository != nil {
		return s.collectionTagsRepository
	}
	s.collectionTagsRepository = &CollectionTagsRepository{
		store: s,
	}
	return s.collectionTagsRepository
}

func (s *Store) ProvaiderItemTags() store.ProvaiderItemTagsRepository {
	if s.provaiderItemTagsRepository != nil {
		return s.provaiderItemTagsRepository
	}
	s.provaiderItemTagsRepository = &ProvaiderItemTagsRepository{
		store: s,
	}
	return s.provaiderItemTagsRepository
}

func (s *Store) Session() store.SessionRepository {
	if s.sessionRepository != nil {
		return s.sessionRepository
	}
	s.sessionRepository = &SessionRepository{
		store: s,
	}
	return s.sessionRepository
}
