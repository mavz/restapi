package sqlstore

import (
	"database/sql"
	"fmt"

	"bitbucket.org/mavz/restapi.git/internal/app/model"
	"bitbucket.org/mavz/restapi.git/internal/app/store"
)

type ProvaiderItemRepository struct {
	store *Store
}

func (r *ProvaiderItemRepository) Create(pi *model.ProvaiderItem) error {

	if err := pi.Validate(); err != nil {
		return err
	}

	if err := pi.BeforeCreate(); err != nil {
		return err
	}
	//убедимся, что родительский элемент существует
	p, err := r.store.Provaider().FindId(pi.ProvaiderId)
	if p == nil || err != nil {
		return err
	}
	//fmt.Println(c)
	result := r.store.db.QueryRow(
		"INSERT INTO provaider_item (id, name, alias, provaider_id) VALUES ($1, $2, $3, $4)  on conflict do nothing returning id",
		pi.ID,
		pi.Name,
		pi.Alias,
		pi.ProvaiderId,
	).Scan(&pi.ID)
	return result
}

func (r *ProvaiderItemRepository) Update(pi *model.ProvaiderItem) error {

	if err := pi.Validate(); err != nil {
		return err
	}
	_, err := r.store.db.Exec(
		"UPDATE provaider_item SET name = $2, alias = $3 WHERE id = $1",
		pi.ID,
		pi.Name,
		pi.Alias,
	)
	return err
}

func (r *ProvaiderItemRepository) Delete(id string) (int, error) {
	res, err := r.store.db.Exec("DELETE FROM provaider_item WHERE id = $1", id)
	if err != nil {
		return 0, store.ErrRecordNotFound
	}
	count, err := res.RowsAffected()
	if err != nil {
		return 0, store.ErrRecordNotFound
	}
	return int(count), nil
}

func (r *ProvaiderItemRepository) FindId(id string) (*model.ProvaiderItem, error) {
	pi := &model.ProvaiderItem{}
	if err := r.store.db.QueryRow(
		"SELECT id, name, alias, provaider_id FROM provaider_item WHERE id = $1",
		id,
	).Scan(
		&pi.ID,
		&pi.Name,
		&pi.Alias,
		&pi.ProvaiderId,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, store.ErrRecordNotFound
		}
		return nil, err
	}

	return pi, nil
}
func (r *ProvaiderItemRepository) FindAllPagination(parent_id string, cs *model.Search) ([]model.ProvaiderItem, error) {

	stringSQL := "SELECT id, name, alias, provaider_id FROM provaider_item  WHERE provaider_id = $1"
	sqlCount := "SELECT count(*) FROM provaider_item  WHERE provaider_id = $1"

	w := " AND (name = $2  OR 1=1)"
	//поиск по всем полям
	if cs.Qstring != "" {
		w = " AND (LOWER(name) LIKE '%' || $2 || '%'  OR LOWER(alias) LIKE '%' || $2 || '%') "
	}
	stringSQL += w
	sqlCount += w

	if err := r.store.db.QueryRow(
		sqlCount, parent_id, cs.Qstring,
	).Scan(
		&cs.CountItemsAll,
	); err != nil {
		fmt.Println("отработать как нибудь2", err)
	}

	sh := &SearchHelper{
		Search: cs,
	}
	//пагинация
	l := sh.GetLIMIT()
	//сортировка
	o := sh.GetORDER()
	stringSQL += o + l

	rows, err := r.store.db.Query(stringSQL, parent_id, cs.Qstring)

	if err != nil {
		return nil, err
	}
	defer rows.Close()
	provaiderItems := []model.ProvaiderItem{}
	for rows.Next() {
		pi := model.ProvaiderItem{}
		err := rows.Scan(&pi.ID, &pi.Name, &pi.Alias, &pi.ProvaiderId)
		if err != nil {
			return nil, err
		}
		provaiderItems = append(provaiderItems, pi)
	}
	return provaiderItems, nil
}

func (r *ProvaiderItemRepository) FindAllPaginationwithTags(parent_id string, cs *model.Search) ([]model.ProvaiderItem, error) {
	provaiderItems := []model.ProvaiderItem{}

	provaiderItems, err := r.store.ProvaiderItem().FindAllPagination(parent_id, cs)
	if err != nil {
		return nil, err
	}

	stringSQL := "SELECT pi.id AS proaider_item_id, t.id, t.name, t.alias"
	stringSQL += " FROM provaider_item AS pi"
	stringSQL += " INNER JOIN provaider_teg AS pt ON pt.provaider_id = pi.id"
	stringSQL += " INNER JOIN  tag AS t ON t.id = pt.tag_id "
	stringSQL += " WHERE pi.provaider_id = $1"

	rows, err := r.store.db.Query(stringSQL, parent_id)

	if err != nil {
		return nil, err
	}
	defer rows.Close()
	//создаем карту-справочник связанных тегов
	tags := map[string][]model.Tag{}
	for rows.Next() {
		t := model.Tag{}
		var pi_id string
		err := rows.Scan(&pi_id, &t.ID, &t.Name, &t.Alias)
		if err != nil {
			return nil, err
		}
		tags[pi_id] = append(tags[pi_id], t)
	}
	if len(tags) <= 0 {
		return provaiderItems, nil
	}
	//подбираем массив тэгов к провайдерам по ключу
	for i, v := range provaiderItems {
		_, ok := tags[v.ID]
		if ok {
			provaiderItems[i].Tags = tags[v.ID]
		}
	}

	return provaiderItems, nil
}

func (r *ProvaiderItemRepository) FindAll(parent_id string) ([]model.ProvaiderItem, error) {

	stringSQL := "SELECT id, name, alias FROM provaider_item  WHERE provaider_id = $1"

	rows, err := r.store.db.Query(stringSQL, parent_id)

	if err != nil {
		return nil, err
	}
	defer rows.Close()
	provaiderItems := []model.ProvaiderItem{}

	for rows.Next() {
		pi := model.ProvaiderItem{}
		err := rows.Scan(&pi.ID, &pi.Name, &pi.Alias, &pi.ProvaiderId)
		if err != nil {
			return nil, err
		}
		provaiderItems = append(provaiderItems, pi)
	}

	return provaiderItems, nil
}
