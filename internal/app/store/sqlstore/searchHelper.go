package sqlstore

import (
	"database/sql"
	"errors"
	"fmt"
	"math"
	"strconv"

	"bitbucket.org/mavz/restapi.git/internal/app/model"
)

var (
	errNotSQL = errors.New("not string SQL")
)

type SearchHelper struct {
	Search *model.Search
}

func (s *SearchHelper) GetWEARE() (string, []interface{}) {
	str := ""
	args := []interface{}{}
	if len(s.Search.Fields) == 0 {
		return str, args
	}
	str = " WEARE "
	i := 0
	fmt.Println(args)
	for k, v := range s.Search.Fields {
		str += " " + k + "=" + "$" + k
		if i < len(s.Search.Fields)-1 {
			str += " AND "
		}
		args = append(args, sql.Named(k, v))
		i++
	}
	fmt.Println("части запроса", str, args)
	/*
		for k, v := range s.Search.Fields {
			str += " " + k + "=" + v
			if i < len(s.Search.Fields)-1 {
				str += " AND "
			}
			i++
		}
		/**/
	return str, args
}

//пагинация
func (s *SearchHelper) GetLIMIT() string {
	//без условия, возвращаем весь результат
	str := ""
	if s.Search.CountItemsPage == 0 {
		return str
	}
	//общее число страниц
	s.Search.CountPages = int(math.Ceil(float64(s.Search.CountItemsAll) / float64(s.Search.CountItemsPage)))
	//первая страница
	str = " LIMIT " + strconv.Itoa(s.Search.CountItemsPage)
	if s.Search.Page == 0 || s.Search.Page == 1 {
		return str
	}
	//если до сих пор неизвестно общее кол-во записей
	//опять возвращаем первую страницу
	if s.Search.CountItemsAll == 0 {
		return str
	}
	//общее число страниц
	//s.Search.CountPages = int(math.Ceil(float64(s.Search.CountItemsAll) / float64(s.Search.CountItemsPage)))
	//корректируем текущую страницу, при необходимости
	if s.Search.Page > s.Search.CountPages {
		s.Search.Page = s.Search.CountPages
	}
	limit := s.Search.CountItemsPage
	offset := (s.Search.Page - 1) * s.Search.CountItemsPage
	//строк на последней странице
	if s.Search.Page == s.Search.CountPages {
		limit = s.Search.CountItemsAll - offset
	}
	str = " LIMIT " + strconv.Itoa(limit) + " OFFSET " + strconv.Itoa(offset)
	return str
}

//сортировка
func (s *SearchHelper) GetORDER() (str string) {
	sort := s.Search.GetStringSort()
	//без условия, возвращаем весь результат
	if sort == "" {
		return
	}
	//по умолчанию сортировка по возрастанию
	sortType := "ASC"
	if s.Search.SortDirection == "DESC" || s.Search.SortDirection == "desc" {
		sortType = "DESC"
	}
	str = " ORDER BY " + sort + " " + sortType
	return
}
