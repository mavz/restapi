package sqlstore

import (
	"database/sql"
	"fmt"

	"bitbucket.org/mavz/restapi.git/internal/app/model"
	"bitbucket.org/mavz/restapi.git/internal/app/store"
)

type TagRepository struct {
	store *Store
}

func (r *TagRepository) Create(t *model.Tag) error {

	if err := t.Validate(); err != nil {
		return err
	}

	if err := t.BeforeCreate(); err != nil {
		return err
	}
	//fmt.Println(c)
	result := r.store.db.QueryRow(
		"INSERT INTO tag (id, name, alias) VALUES ($1, $2, $3)  on conflict do nothing returning id",
		t.ID,
		t.Name,
		t.Alias,
	).Scan(&t.ID)
	return result
}

func (r *TagRepository) Update(t *model.Tag) error {

	if err := t.Validate(); err != nil {
		return err
	}
	_, err := r.store.db.Exec(
		"UPDATE tag SET name = $2, alias = $3 WHERE id = $1",
		t.ID,
		t.Name,
		t.Alias,
	)
	return err
}

func (r *TagRepository) Delete(id string) (int, error) {
	tx, err := r.store.db.Begin()
	res, err := r.store.db.Exec("DELETE FROM tag WHERE id = $1", id)
	if err != nil {
		tx.Rollback()
		return 0, err
	}
	if _, err := r.store.db.Exec("DELETE FROM tag_collection WHERE tag_id = $1", id); err != nil {
		tx.Rollback()
		return 0, err
	}
	if _, err := r.store.db.Exec("DELETE FROM provaider_teg WHERE tag_id = $1", id); err != nil {
		tx.Rollback()
		return 0, err
	}
	if err := tx.Commit(); err != nil {
		return 0, err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}

	return int(count), nil
}

func (r *TagRepository) FindId(id string) (*model.Tag, error) {
	t := &model.Tag{}
	if err := r.store.db.QueryRow(
		"SELECT id, name, alias FROM tag WHERE id = $1",
		id,
	).Scan(
		&t.ID,
		&t.Name,
		&t.Alias,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, store.ErrRecordNotFound
		}
		return nil, err
	}

	return t, nil
}
func (r *TagRepository) FindAllPagination(cs *model.Search) ([]model.Tag, error) {

	stringSQL := "SELECT id, name, alias FROM tag"
	sqlCount := "SELECT count(*) FROM tag"

	sh := &SearchHelper{
		Search: cs,
	}

	w := " WHERE (name = $1  OR 1=1)"
	//поиск по всем полям
	if cs.Qstring != "" {
		w = " WHERE (LOWER(name) LIKE '%' || $1 || '%'  OR LOWER(alias) LIKE '%' || $1 || '%') "
	}
	stringSQL += w
	sqlCount += w

	if err := r.store.db.QueryRow(
		sqlCount, cs.Qstring,
	).Scan(
		&cs.CountItemsAll,
	); err != nil {
		fmt.Println("отработать как нибудь2", err)
	}

	//пагинация
	l := sh.GetLIMIT()
	//сортировка
	o := sh.GetORDER()
	stringSQL += o + l
	//fmt.Println(stringSQL)

	rows, err := r.store.db.Query(stringSQL, cs.Qstring)

	if err != nil {
		return nil, err
	}
	defer rows.Close()
	tags := []model.Tag{}

	for rows.Next() {
		t := model.Tag{}
		err := rows.Scan(&t.ID, &t.Name, &t.Alias)
		if err != nil {
			return nil, err
		}
		tags = append(tags, t)
	}

	return tags, nil
}

func (r *TagRepository) FindAll() ([]model.Tag, error) {

	stringSQL := "SELECT id, name, alias FROM tag"

	rows, err := r.store.db.Query(stringSQL)

	if err != nil {
		return nil, err
	}
	defer rows.Close()
	tags := []model.Tag{}

	for rows.Next() {
		t := model.Tag{}
		err := rows.Scan(&t.ID, &t.Name, &t.Alias)
		if err != nil {
			return nil, err
		}
		tags = append(tags, t)
	}

	return tags, nil
}
