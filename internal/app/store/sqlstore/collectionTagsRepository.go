package sqlstore

import (
	"database/sql"
	"fmt"

	"bitbucket.org/mavz/restapi.git/internal/app/model"
	"bitbucket.org/mavz/restapi.git/internal/app/store"
)

type CollectionTagsRepository struct {
	store *Store
}

func (r *CollectionTagsRepository) DeleteTag(ct *model.CollectionTag) (int, error) {
	if err := ct.Validate(); err != nil {
		return 0, err
	}
	res, err := r.store.db.Exec("DELETE FROM tag_collection WHERE tag_id = $1 AND collection_id = $2", ct.TagId, ct.CollectionId)
	if err != nil {
		return 0, store.ErrRecordNotFound
	}
	count, err := res.RowsAffected()
	if err != nil {
		return 0, store.ErrRecordNotFound
	}
	return int(count), nil
	return 0, nil
}

func (r *CollectionTagsRepository) FindAllTags(collectionId string) ([]model.Tag, error) {
	stringSQL := "SELECT t.id, t.name, t.alias"
	stringSQL += " FROM tag AS t "
	stringSQL += " INNER JOIN tag_collection AS tc ON tc.tag_id = t.id"
	stringSQL += " WHERE tc.collection_id = $1"

	fmt.Println(stringSQL)
	rows, err := r.store.db.Query(stringSQL, collectionId)

	if err != nil {
		return nil, err
	}
	defer rows.Close()
	tags := []model.Tag{}

	for rows.Next() {
		t := model.Tag{}
		err := rows.Scan(&t.ID, &t.Name, &t.Alias)
		if err != nil {
			return nil, err
		}
		tags = append(tags, t)
	}

	return tags, nil
}

func (r *CollectionTagsRepository) FindAllTagsPagination(collectionId string, cs *model.Search) ([]model.Tag, error) {

	stringSQL := "SELECT t.id, t.name, t.alias"
	stringSQL += " FROM tag AS t "
	stringSQL += " INNER JOIN tag_collection AS tc ON tc.tag_id = t.id"
	stringSQL += " WHERE tc.collection_id = $1"

	sqlCount := "SELECT count(*), t.name, t.alias"
	sqlCount += " FROM tag AS t "
	sqlCount += " INNER JOIN tag_collection AS tc ON tc.tag_id = t.id"
	sqlCount += " WHERE tc.collection_id = $1"

	w := " AND (name = $2  OR 1=1)"
	//поиск по всем полям
	if cs.Qstring != "" {
		w = " AND (LOWER(t.name) LIKE '%' || $2 || '%'  OR LOWER(t.alias) LIKE '%' || $2 || '%') "
	}
	stringSQL += w
	sqlCount += w

	if err := r.store.db.QueryRow(
		sqlCount, collectionId, cs.Qstring,
	).Scan(
		&cs.CountItemsAll,
	); err != nil {
		fmt.Println("отработать как нибудь2", err)
	}

	sh := &SearchHelper{
		Search: cs,
	}
	//пагинация
	l := sh.GetLIMIT()
	//сортировка
	o := sh.GetORDER()
	stringSQL += o + l
	fmt.Println(stringSQL)

	rows, err := r.store.db.Query(stringSQL, collectionId, cs.Qstring)

	if err != nil {
		return nil, err
	}
	defer rows.Close()
	tags := []model.Tag{}

	for rows.Next() {
		t := model.Tag{}
		err := rows.Scan(&t.ID, &t.Name, &t.Alias)
		if err != nil {
			return nil, err
		}
		tags = append(tags, t)
	}

	return tags, nil
}

func (r *CollectionTagsRepository) FindSelectTags(collectionId string, q string) ([]model.Tag, error) {
	tags := []model.Tag{}
	//if q == "" {
	//	tags, err := r.store.Tag().FindAll()
	//	//TODO  убираем уже имеющиеся у коллекции
	//	return tags, err
	//}

	stringSQL := "SELECT id, name, alias"
	stringSQL += " FROM tag "
	if q != "" {
		stringSQL += " WHERE (LOWER(name) LIKE '%' || $1 || '%'  OR LOWER(alias) LIKE '%' || $1 || '%')"
	} else {
		stringSQL += " WHERE (name = $1  OR 1=1)"
	}
	stringSQL += " AND id NOT IN ("
	stringSQL += " SELECT t.id "
	stringSQL += " FROM tag AS t "
	stringSQL += " INNER JOIN tag_collection AS tc ON tc.tag_id = t.id"
	stringSQL += " WHERE tc.collection_id = $2)"

	rows, err := r.store.db.Query(stringSQL, q, collectionId)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		t := model.Tag{}
		err := rows.Scan(&t.ID, &t.Name, &t.Alias)
		if err != nil {
			return nil, err
		}
		tags = append(tags, t)
	}

	return tags, nil
}
func (r *CollectionTagsRepository) AddTag(ct *model.CollectionTag) error {

	if err := ct.Validate(); err != nil {
		return err
	}
	tx, err := r.store.db.Begin()
	if err != nil {
		return err
	}
	isTag := "f"
	if err := r.store.db.QueryRow(
		"SELECT id FROM tag WHERE id=$1",
		ct.TagId,
	).Scan(
		isTag,
	); err != nil {
		if err == sql.ErrNoRows {
			tx.Rollback()
		}
	}
	isCollection := "f"
	if err := r.store.db.QueryRow(
		"SELECT id FROM collection WHERE id=$1",
		ct.CollectionId,
	).Scan(
		isCollection,
	); err != nil {
		if err == sql.ErrNoRows {
			tx.Rollback()
		}
	}
	if _, err := r.store.db.Exec("DELETE FROM tag_collection WHERE tag_id = $1 AND collection_id = $2",
		ct.TagId,
		ct.CollectionId,
	); err != nil {
		tx.Rollback()
		return err
	}
	if _, err := r.store.db.Exec(
		"INSERT INTO tag_collection (tag_id, collection_id) VALUES ($1, $2)",
		ct.TagId,
		ct.CollectionId,
	); err != nil {
		tx.Rollback()
		return err
	}
	if err := tx.Commit(); err != nil {
		return err
	}

	return nil

}
