package sqlstore

import (
	"database/sql"
	"fmt"

	"bitbucket.org/mavz/restapi.git/internal/app/model"
	"bitbucket.org/mavz/restapi.git/internal/app/store"
)

type ProvaiderItemTagsRepository struct {
	store *Store
}

func (r *ProvaiderItemTagsRepository) DeleteTag(pit *model.ProvaiderItemTag) (int, error) {
	/**/
	if err := pit.Validate(); err != nil {
		return 0, err
	}
	fmt.Println("Параметр из URL ", pit)
	res, err := r.store.db.Exec("DELETE FROM provaider_teg WHERE tag_id = $1 AND provaider_id = $2", pit.TagId, pit.ProvaiderId)
	if err != nil {
		return 0, store.ErrRecordNotFound
	}
	count, err := res.RowsAffected()
	if err != nil {
		return 0, store.ErrRecordNotFound
	}
	return int(count), nil
	/**/
	//return 0, nil
}

//TODO реализовать
func (r *ProvaiderItemTagsRepository) FindAllTags(provaiderItemId string) ([]model.Tag, error) {
	/*	stringSQL := "SELECT t.id, t.name, t.alias"
		stringSQL += " FROM tag AS t "
		stringSQL += " INNER JOIN tag_collection AS tc ON tc.tag_id = t.id"
		stringSQL += " WHERE tc.collection_id = $1"

		rows, err := r.store.db.Query(stringSQL, provaiderItemId)

		if err != nil {
			return nil, err
		}
		defer rows.Close()/**/
	tags := []model.Tag{}
	/*
		for rows.Next() {
			t := model.Tag{}
			err := rows.Scan(&t.ID, &t.Name, &t.Alias)
			if err != nil {
				return nil, err
			}
			tags = append(tags, t)
		}
	/**/
	return tags, nil
}

func (r *ProvaiderItemTagsRepository) FindAllTagsPagination(provaiderItemId string, cs *model.Search) ([]model.Tag, error) {

	stringSQL := "SELECT t.id, t.name, t.alias"
	stringSQL += " FROM tag AS t "
	stringSQL += " INNER JOIN provaider_teg AS pt ON pt.tag_id = t.id"
	stringSQL += " WHERE pt.provaider_id = $1"

	sqlCount := "SELECT count(*), t.name, t.alias"
	sqlCount += " FROM tag AS t "
	sqlCount += " INNER JOIN provaider_teg AS pt ON pt.tag_id = t.id"
	sqlCount += " WHERE pt.provaider_id = $1"

	if err := r.store.db.QueryRow(
		sqlCount, provaiderItemId,
	).Scan(
		&cs.CountItemsAll,
	); err != nil {
		fmt.Println("отработать как нибудь2", err)
	}

	sh := &SearchHelper{
		Search: cs,
	}
	//пагинация
	l := sh.GetLIMIT()
	//сортировка
	o := sh.GetORDER()
	stringSQL += o + l
	fmt.Println(stringSQL)

	rows, err := r.store.db.Query(stringSQL, provaiderItemId)

	if err != nil {
		return nil, err
	}
	defer rows.Close()
	tags := []model.Tag{}

	for rows.Next() {
		t := model.Tag{}
		err := rows.Scan(&t.ID, &t.Name, &t.Alias)
		if err != nil {
			return nil, err
		}
		tags = append(tags, t)
	}

	return tags, nil
}

func (r *ProvaiderItemTagsRepository) FindSelectTags(provaiderItemId string, collectionId string, q string) ([]model.Tag, error) {
	tags := []model.Tag{}

	stringSQL := "SELECT  DISTINCT t.id, t.name, t.alias"
	stringSQL += " FROM tag AS t"
	stringSQL += " LEFT JOIN tag_collection AS tc ON tc.tag_id = t.id"
	if q != "" {
		stringSQL += " WHERE (LOWER(t.name) LIKE '%' || $1 || '%'  OR LOWER(t.alias) LIKE '%' || $1 || '%')"
	} else {
		stringSQL += " WHERE (t.name = $1  OR 1=1)" //заглушка для использования переменной $1
	}
	if collectionId != "" {
		stringSQL += " AND tc.collection_id = $2"
	} else {
		stringSQL += " AND (t.name = $2  OR 1=1)" //заглушка для использования переменной $2
	}
	stringSQL += " AND id NOT IN ("
	stringSQL += " SELECT t.id "
	stringSQL += " FROM tag AS t "
	stringSQL += " INNER JOIN provaider_teg AS pt ON pt.tag_id = t.id"
	stringSQL += " WHERE pt.provaider_id = $3)"

	fmt.Println("запрос провайдер, коллекции, теги ", collectionId, stringSQL)

	rows, err := r.store.db.Query(stringSQL, q, collectionId, provaiderItemId)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		t := model.Tag{}
		err := rows.Scan(&t.ID, &t.Name, &t.Alias)
		if err != nil {
			return nil, err
		}
		tags = append(tags, t)
	}

	return tags, nil
}

//TODO реализовать
func (r *ProvaiderItemTagsRepository) AddTag(pit *model.ProvaiderItemTag) error {
	/**/

	if err := pit.Validate(); err != nil {
		return err
	}
	tx, err := r.store.db.Begin()
	if err != nil {
		return err
	}
	isTag := "f"
	if err := r.store.db.QueryRow(
		"SELECT id FROM tag WHERE id=$1",
		pit.TagId,
	).Scan(
		isTag,
	); err != nil {
		if err == sql.ErrNoRows {
			tx.Rollback()
		}
	}
	isProvaider := "f"
	if err := r.store.db.QueryRow(
		"SELECT id FROM provaider_item WHERE id=$1",
		pit.ProvaiderId,
	).Scan(
		isProvaider,
	); err != nil {
		if err == sql.ErrNoRows {
			tx.Rollback()
		}
	}
	if _, err := r.store.db.Exec("DELETE FROM provaider_teg WHERE tag_id = $1 AND provaider_id = $2",
		pit.TagId,
		pit.ProvaiderId,
	); err != nil {
		tx.Rollback()
		return err
	}
	if _, err := r.store.db.Exec(
		"INSERT INTO provaider_teg (tag_id, provaider_id) VALUES ($1, $2)",
		pit.TagId,
		pit.ProvaiderId,
	); err != nil {
		tx.Rollback()
		return err
	}
	if err := tx.Commit(); err != nil {
		return err
	}

	return nil

}
