package sqlstore

import (
	"database/sql"
	"fmt"

	"bitbucket.org/mavz/restapi.git/internal/app/model"
	"bitbucket.org/mavz/restapi.git/internal/app/store"
)

type ProvaiderRepository struct {
	store *Store
}

func (r *ProvaiderRepository) Create(p *model.Provaider) error {

	if err := p.Validate(); err != nil {
		return err
	}

	if err := p.BeforeCreate(); err != nil {
		return err
	}

	result := r.store.db.QueryRow(
		"INSERT INTO provaider (id, name) VALUES ($1, $2)  on conflict do nothing returning id",
		p.ID,
		p.Name,
	).Scan(&p.ID)
	return result
}

func (r *ProvaiderRepository) Update(p *model.Provaider) error {

	if err := p.Validate(); err != nil {
		return err
	}
	_, err := r.store.db.Exec(
		"UPDATE provaider SET name = $2 WHERE id = $1",
		p.ID,
		p.Name,
	)
	return err
}

func (r *ProvaiderRepository) Delete(id string) (int, error) {

	tx, err := r.store.db.Begin()
	res, err := r.store.db.Exec("DELETE FROM provaider WHERE id = $1", id)
	if err != nil {
		tx.Rollback()
		return 0, err
	}
	if _, err := r.store.db.Exec("DELETE FROM provaider_item WHERE provaider_id = $1", id); err != nil {
		tx.Rollback()
		return 0, err
	}
	if err := tx.Commit(); err != nil {
		return 0, err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}
	return int(count), nil
}

func (r *ProvaiderRepository) FindId(id string) (*model.Provaider, error) {
	p := &model.Provaider{}
	if err := r.store.db.QueryRow(
		"SELECT id, name FROM provaider WHERE id = $1",
		id,
	).Scan(
		&p.ID,
		&p.Name,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, store.ErrRecordNotFound
		}
		return nil, err
	}

	return p, nil
}
func (r *ProvaiderRepository) FindAllPagination(cs *model.Search) ([]model.Provaider, error) {

	stringSQL := "SELECT id, name FROM provaider"
	sqlCount := "SELECT count(*) FROM provaider"

	sh := &SearchHelper{
		Search: cs,
	}
	w := " WHERE (name = $1  OR 1=1)"
	//поиск по всем полям
	if cs.Qstring != "" {
		w = " WHERE LOWER(name) LIKE '%' || $1 || '%' "
	}
	stringSQL += w
	sqlCount += w
	if err := r.store.db.QueryRow(
		sqlCount, cs.Qstring,
	).Scan(
		&cs.CountItemsAll,
	); err != nil {
		fmt.Println("отработать как нибудь2", err)
	}

	//пагинация
	l := sh.GetLIMIT()
	//сортировка
	o := sh.GetORDER()
	stringSQL += o + l
	fmt.Println(stringSQL)

	rows, err := r.store.db.Query(stringSQL, cs.Qstring)

	if err != nil {
		return nil, err
	}
	defer rows.Close()
	provaiders := []model.Provaider{}

	for rows.Next() {
		p := model.Provaider{}
		err := rows.Scan(&p.ID, &p.Name)
		if err != nil {
			return nil, err
		}
		provaiders = append(provaiders, p)
	}

	return provaiders, nil
}

func (r *ProvaiderRepository) FindAll() ([]model.Provaider, error) {

	stringSQL := "SELECT id, name FROM provaider"

	rows, err := r.store.db.Query(stringSQL)

	if err != nil {
		return nil, err
	}
	defer rows.Close()
	provaiders := []model.Provaider{}

	for rows.Next() {
		p := model.Provaider{}
		err := rows.Scan(&p.ID, &p.Name)
		if err != nil {
			return nil, err
		}
		provaiders = append(provaiders, p)
	}

	return provaiders, nil
}
