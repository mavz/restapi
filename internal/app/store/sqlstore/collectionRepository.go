package sqlstore

import (
	"database/sql"
	"fmt"

	"bitbucket.org/mavz/restapi.git/internal/app/model"
	"bitbucket.org/mavz/restapi.git/internal/app/store"
)

type CollectionRepository struct {
	store *Store
}

func (r *CollectionRepository) Create(c *model.Collection) error {

	if err := c.Validate(); err != nil {
		return err
	}

	if err := c.BeforeCreate(); err != nil {
		return err
	}
	//fmt.Println(c)
	result := r.store.db.QueryRow(
		"INSERT INTO collection (id, name, alias) VALUES ($1, $2, $3)  on conflict do nothing returning id",
		c.ID,
		c.Name,
		c.Alias,
	).Scan(&c.ID)
	return result
}

func (r *CollectionRepository) Update(c *model.Collection) error {

	if err := c.Validate(); err != nil {
		return err
	}
	_, err := r.store.db.Exec(
		"UPDATE collection SET name = $2, alias = $3 WHERE id = $1",
		c.ID,
		c.Name,
		c.Alias,
	)
	return err
}

func (r *CollectionRepository) Delete(id string) (int, error) {

	tx, err := r.store.db.Begin()
	res, err := r.store.db.Exec("DELETE FROM collection WHERE id = $1", id)
	if err != nil {
		tx.Rollback()
		return 0, err
	}
	if _, err := r.store.db.Exec("DELETE FROM tag_collection WHERE collection_id = $1", id); err != nil {
		tx.Rollback()
		return 0, err
	}
	if err := tx.Commit(); err != nil {
		return 0, err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}
	return int(count), nil
}

func (r *CollectionRepository) FindId(id string) (*model.Collection, error) {
	c := &model.Collection{}
	if err := r.store.db.QueryRow(
		"SELECT id, name, alias FROM collection WHERE id = $1",
		id,
	).Scan(
		&c.ID,
		&c.Name,
		&c.Alias,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, store.ErrRecordNotFound
		}
		return nil, err
	}

	return c, nil
}
func (r *CollectionRepository) FindAllPagination(cs *model.Search) ([]model.Collection, error) {

	stringSQL := "SELECT id, name, alias FROM collection "
	sqlCount := "SELECT count(*) FROM collection "
	sh := &SearchHelper{
		Search: cs,
	}
	w := " WHERE (name = $1  OR 1=1)"
	//поиск по всем полям
	if cs.Qstring != "" {
		w = " WHERE (LOWER(name) LIKE '%' || $1 || '%'  OR LOWER(alias) LIKE '%' || $1 || '%') "
	}
	stringSQL += w
	sqlCount += w

	if err := r.store.db.QueryRow(
		sqlCount, cs.Qstring,
	).Scan(
		&cs.CountItemsAll,
	); err != nil {
		fmt.Println("отработать как нибудь2", err)
	}

	//пагинация
	l := sh.GetLIMIT()
	//сортировка
	o := sh.GetORDER()
	stringSQL += o + l
	//fmt.Println(stringSQL)

	rows, err := r.store.db.Query(stringSQL, cs.Qstring)

	if err != nil {
		return nil, err
	}
	defer rows.Close()
	collections := []model.Collection{}

	for rows.Next() {
		c := model.Collection{}
		err := rows.Scan(&c.ID, &c.Name, &c.Alias)
		if err != nil {
			return nil, err
		}
		collections = append(collections, c)
	}

	return collections, nil
}

func (r *CollectionRepository) FindAll() ([]model.Collection, error) {

	stringSQL := "SELECT id, name, alias FROM collection"

	rows, err := r.store.db.Query(stringSQL)

	if err != nil {
		return nil, err
	}
	defer rows.Close()
	collections := []model.Collection{}

	for rows.Next() {
		c := model.Collection{}
		err := rows.Scan(&c.ID, &c.Name, &c.Alias)
		if err != nil {
			return nil, err
		}
		collections = append(collections, c)
	}

	return collections, nil
}
