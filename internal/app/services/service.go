package service

import (
	"time"

	"bitbucket.org/mavz/restapi.git/internal/app/model"
	"bitbucket.org/mavz/restapi.git/internal/app/store"
	"bitbucket.org/mavz/restapi.git/pkg/auth"
	"bitbucket.org/mavz/restapi.git/pkg/rmq"
)

type Tokens struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

type SearchRequest struct {
	Page           string `json:"page"`
	Sort           string `json:"sort"`
	SortDirection  string `json:"sortDirection"`
	CountItemsPage string `json:"countItemsPage"`
	Qstring        string `json:"q"`
}

type Users interface {
	SignUp(userId int) error
	SignIn(email string, pass string) (Tokens, error)
	RefreshTokens(refreshToken string) (Tokens, error)
	CreateUser(user *model.User) error
}

type Collections interface {
	Create(c *model.Collection) error
	Update(c *model.Collection) (*model.Collection, error)
	Delete(id string) error
	FindId(id string) (*model.Collection, error)
	FindAll() ([]model.Collection, error)
	FindAllPagination(cs *model.Search) ([]model.Collection, error)
}

type Tags interface {
	Create(c *model.Tag) error
	Update(c *model.Tag) (*model.Tag, error)
	Delete(id string) error
	FindId(id string) (*model.Tag, error)
	FindAll() ([]model.Tag, error)
	FindAllPagination(cs *model.Search) ([]model.Tag, error)
}

type Provaiders interface {
	Create(c *model.Provaider) error
	Update(c *model.Provaider) (*model.Provaider, error)
	Delete(id string) error
	FindId(id string) (*model.Provaider, error)
	FindAll() ([]model.Provaider, error)
	FindAllPagination(cs *model.Search) ([]model.Provaider, error)
}

type ProvaiderItems interface {
	Create(c *model.ProvaiderItem) error
	Update(c *model.ProvaiderItem) (*model.ProvaiderItem, error)
	Delete(id string) error
	FindId(id string) (*model.ProvaiderItem, error)
	FindAll(parentId string) ([]model.ProvaiderItem, error)
	FindAllPagination(parentId string, cs *model.Search) ([]model.ProvaiderItem, error)
}

type CollectionsTags interface {
	FindAllTags(collectionId string) ([]model.Tag, error)
	FindAllTagsPagination(collectionId string, cs *model.Search) ([]model.Tag, error)
	FindSelectTags(collectionId string, q string) ([]model.Tag, error)
	AddTag(ct *model.CollectionTag) error
	DeleteTag(ct *model.CollectionTag) error
}

type ProvaidersItemsTags interface {
	FindAllTagsPagination(provaiderItemId string, cs *model.Search) ([]model.Tag, error)
	FindSelectTags(provaideritemId string, collectionId string, q string) ([]model.Tag, error)
	AddTag(pit *model.ProvaiderItemTag) error
	DeleteTag(pit *model.ProvaiderItemTag) error
}

type TestRabbitPublisher interface {
	AddRandomInts()
}

type Services struct {
	Users               Users
	Collections         Collections
	Tags                Tags
	Provaiders          Provaiders
	ProvaiderItems      ProvaiderItems
	CollectionsTags     CollectionsTags
	ProvaidersItemsTags ProvaidersItemsTags
	TestRabbitPublisher TestRabbitPublisher
}

type Deps struct {
	Store           store.Store
	TokenManager    auth.TokenManager
	AccessTokenTTL  time.Duration
	RefreshTokenTTL time.Duration
	RabbitClient    rmq.RabbitClient
}

func NewServices(deps Deps) *Services {

	usersService := NewUsersService(deps.Store, deps.TokenManager, deps.AccessTokenTTL, deps.RefreshTokenTTL)
	collectionsService := NewCollectionsService(deps.Store)
	tagsService := NewTagsService(deps.Store)
	provaidersService := NewProvaidersService(deps.Store)
	provaiderItemsService := NewProvaiderItemsService(deps.Store)
	collectionsTagsService := NewCollectionsTagsService(deps.Store)
	provaidersItemsTagsService := NewProvaidersItemsTagsService(deps.Store)
	testRabbitPublisherService := NewTestRabbitPublisherService(deps.RabbitClient)

	return &Services{
		Users:               usersService,
		Collections:         collectionsService,
		Tags:                tagsService,
		Provaiders:          provaidersService,
		ProvaiderItems:      provaiderItemsService,
		CollectionsTags:     collectionsTagsService,
		ProvaidersItemsTags: provaidersItemsTagsService,
		TestRabbitPublisher: testRabbitPublisherService,
	}
}
