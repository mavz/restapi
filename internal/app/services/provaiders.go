package service

import (
	"bitbucket.org/mavz/restapi.git/internal/app/model"
	"bitbucket.org/mavz/restapi.git/internal/app/store"
)

type ProvaidersService struct {
	store store.Store
}

func NewProvaidersService(store store.Store) *ProvaidersService {
	return &ProvaidersService{
		store: store,
	}
}

func (s *ProvaidersService) Create(p *model.Provaider) error {
	if err := s.store.Provaider().Create(p); err != nil {
		return err
	}
	return nil
}

func (s *ProvaidersService) Update(p *model.Provaider) (*model.Provaider, error) {

	provaider, err := s.store.Provaider().FindId(p.ID)
	if err != nil {
		return nil, err
	}

	if p.Name != "" {
		provaider.Name = p.Name
	}

	if err := s.store.Provaider().Update(provaider); err != nil {
		return nil, err
	}
	return provaider, nil
}

func (s *ProvaidersService) Delete(id string) error {
	if _, err := s.store.Provaider().Delete(id); err != nil {
		return err
	}
	return nil
}

func (s *ProvaidersService) FindId(id string) (*model.Provaider, error) {

	p, err := s.store.Provaider().FindId(id)
	if err != nil {
		return nil, err
	}
	return p, nil
}

func (s *ProvaidersService) FindAll() ([]model.Provaider, error) {

	provaiders, err := s.store.Provaider().FindAll()
	if err != nil {
		return nil, err
	}
	return provaiders, nil
}
func (s *ProvaidersService) FindAllPagination(cs *model.Search) ([]model.Provaider, error) {

	provaiders, err := s.store.Provaider().FindAllPagination(cs)
	if err != nil {
		return nil, err
	}
	return provaiders, nil
}
