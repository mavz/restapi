package service

import (
	"bitbucket.org/mavz/restapi.git/internal/app/model"
	"bitbucket.org/mavz/restapi.git/internal/app/store"
)

type ProvaiderItemsService struct {
	store store.Store
}

func NewProvaiderItemsService(store store.Store) *ProvaiderItemsService {
	return &ProvaiderItemsService{
		store: store,
	}
}

func (s *ProvaiderItemsService) Create(pi *model.ProvaiderItem) error {
	if err := s.store.ProvaiderItem().Create(pi); err != nil {
		return err
	}
	return nil
}

func (s *ProvaiderItemsService) Update(pi *model.ProvaiderItem) (*model.ProvaiderItem, error) {

	provaiderItem, err := s.store.ProvaiderItem().FindId(pi.ID)
	if err != nil {
		return nil, err
	}

	if pi.Name != "" {
		provaiderItem.Name = pi.Name
	}

	if err := s.store.ProvaiderItem().Update(provaiderItem); err != nil {
		return nil, err
	}
	return provaiderItem, nil
}

func (s *ProvaiderItemsService) Delete(id string) error {
	if _, err := s.store.ProvaiderItem().Delete(id); err != nil {
		return err
	}
	return nil
}

func (s *ProvaiderItemsService) FindId(id string) (*model.ProvaiderItem, error) {

	pi, err := s.store.ProvaiderItem().FindId(id)
	if err != nil {
		return nil, err
	}
	return pi, nil
}

func (s *ProvaiderItemsService) FindAll(parentId string) ([]model.ProvaiderItem, error) {

	provaiderItems, err := s.store.ProvaiderItem().FindAll(parentId)
	if err != nil {
		return nil, err
	}
	return provaiderItems, nil
}

func (s *ProvaiderItemsService) FindAllPagination(parentId string, cs *model.Search) ([]model.ProvaiderItem, error) {

	provaiderItems, err := s.store.ProvaiderItem().FindAllPagination(parentId, cs)
	if err != nil {
		return nil, err
	}
	return provaiderItems, nil
}
