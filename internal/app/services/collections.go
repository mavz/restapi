package service

import (
	"bitbucket.org/mavz/restapi.git/internal/app/model"
	"bitbucket.org/mavz/restapi.git/internal/app/store"
	"bitbucket.org/mavz/restapi.git/pkg/logger"
)

type CollectionsService struct {
	store store.Store
}

func NewCollectionsService(store store.Store) *CollectionsService {
	return &CollectionsService{
		store: store,
	}
}

func (s *CollectionsService) Create(c *model.Collection) error {
	if err := s.store.Collection().Create(c); err != nil {
		return err
	}
	return nil
}

func (s *CollectionsService) Update(c *model.Collection) (*model.Collection, error) {

	collection, err := s.store.Collection().FindId(c.ID)
	if err != nil {
		return nil, err
	}

	if c.Name != "" {
		collection.Name = c.Name
	}
	if c.Alias != "" {
		collection.Alias = c.Alias
	}

	if err := s.store.Collection().Update(collection); err != nil {
		return nil, err
	}
	return collection, nil
}

func (s *CollectionsService) Delete(id string) error {
	if _, err := s.store.Collection().Delete(id); err != nil {
		return err
	}
	return nil
}

func (s *CollectionsService) FindId(id string) (*model.Collection, error) {

	c, err := s.store.Collection().FindId(id)
	if err != nil {
		return nil, err
	}
	return c, nil
}

func (s *CollectionsService) FindAll() ([]model.Collection, error) {

	collections, err := s.store.Collection().FindAll()
	if err != nil {
		return nil, err
	}
	return collections, nil
}
func (s *CollectionsService) FindAllPagination(cs *model.Search) ([]model.Collection, error) {
	logger.Info("Ищем данные в сервисе с пагинацией: ", cs)
	collections, err := s.store.Collection().FindAllPagination(cs)
	if err != nil {
		return nil, err
	}
	return collections, nil
}
