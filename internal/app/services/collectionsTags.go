package service

import (
	"bitbucket.org/mavz/restapi.git/internal/app/model"
	"bitbucket.org/mavz/restapi.git/internal/app/store"
)

type CollectionsTagsService struct {
	store store.Store
}

func NewCollectionsTagsService(store store.Store) *CollectionsTagsService {
	return &CollectionsTagsService{
		store: store,
	}
}

func (ct *CollectionsTagsService) FindAllTags(collectionId string) ([]model.Tag, error) {
	tags, err := ct.store.CollectionTags().FindAllTags(collectionId)
	if err != nil {
		return nil, err
	}
	return tags, nil
}

func (ct *CollectionsTagsService) FindAllTagsPagination(collectionId string, cs *model.Search) ([]model.Tag, error) {
	tags, err := ct.store.CollectionTags().FindAllTagsPagination(collectionId, cs)
	if err != nil {
		return nil, err
	}
	return tags, nil
}

func (ct *CollectionsTagsService) FindSelectTags(collectionId string, q string) ([]model.Tag, error) {
	tags, err := ct.store.CollectionTags().FindSelectTags(collectionId, q)
	if err != nil {
		return nil, err
	}
	return tags, nil
}

func (ct *CollectionsTagsService) AddTag(m *model.CollectionTag) error {
	if err := ct.store.CollectionTags().AddTag(m); err != nil {
		return err
	}
	return nil
}

func (ct *CollectionsTagsService) DeleteTag(m *model.CollectionTag) error {
	_, err := ct.store.CollectionTags().DeleteTag(m)
	if err != nil {
		return err
	}
	return nil
}
