package service

import (
	"encoding/json"
	"math/rand"
	"time"

	"bitbucket.org/mavz/restapi.git/pkg/logger"
	"bitbucket.org/mavz/restapi.git/pkg/rmq"
)

type AddTask struct {
	Number1 int
	Number2 int
}

type TestRabbitPublisherService struct {
	rabbit rmq.RabbitClient
}

func NewTestRabbitPublisherService(rabbit rmq.RabbitClient) *TestRabbitPublisherService {
	return &TestRabbitPublisherService{
		rabbit: rabbit,
	}
}

func (s *TestRabbitPublisherService) AddRandomInts() {
	/*
		logger.Info("Добавляем задания: ", "add")
		err := s.rabbit.TestAdd()
		if err != nil {
			logger.Info("AddRandomInts что-то пошло не так: ", err)
		}
	*/
	/**/
	//var rc rmq.RabbitClient
	//rc.Consume("test-queue", funcName)
	rand.Seed(time.Now().UnixNano())
	addTask := AddTask{Number1: rand.Intn(999), Number2: rand.Intn(999)}
	body, err := json.Marshal(addTask)
	if err != nil {
		logger.Info("Error encoding JSON: ", err)
	}

	s.rabbit.Publish("test-queue", body)
	/**/
}
