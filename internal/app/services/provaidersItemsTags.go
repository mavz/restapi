package service

import (
	"bitbucket.org/mavz/restapi.git/internal/app/model"
	"bitbucket.org/mavz/restapi.git/internal/app/store"
)

type ProvaidersItemsTagsService struct {
	store store.Store
}

func NewProvaidersItemsTagsService(store store.Store) *ProvaidersItemsTagsService {
	return &ProvaidersItemsTagsService{
		store: store,
	}
}

func (ct *ProvaidersItemsTagsService) FindAllTagsPagination(provaiderItemId string, cs *model.Search) ([]model.Tag, error) {
	tags, err := ct.store.ProvaiderItemTags().FindAllTagsPagination(provaiderItemId, cs)
	if err != nil {
		return nil, err
	}
	return tags, nil
}

func (ct *ProvaidersItemsTagsService) FindSelectTags(provaideritemId string, collectionId string, q string) ([]model.Tag, error) {
	tags, err := ct.store.ProvaiderItemTags().FindSelectTags(provaideritemId, collectionId, q)
	if err != nil {
		return nil, err
	}
	return tags, nil
}

func (ct *ProvaidersItemsTagsService) AddTag(m *model.ProvaiderItemTag) error {
	if err := ct.store.ProvaiderItemTags().AddTag(m); err != nil {
		return err
	}
	return nil
}

func (ct *ProvaidersItemsTagsService) DeleteTag(m *model.ProvaiderItemTag) error {
	_, err := ct.store.ProvaiderItemTags().DeleteTag(m)
	if err != nil {
		return err
	}
	return nil
}
