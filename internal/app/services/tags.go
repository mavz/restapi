package service

import (
	"bitbucket.org/mavz/restapi.git/internal/app/model"
	"bitbucket.org/mavz/restapi.git/internal/app/store"
)

type TagsService struct {
	store store.Store
}

func NewTagsService(store store.Store) *TagsService {
	return &TagsService{
		store: store,
	}
}

func (s *TagsService) Create(t *model.Tag) error {
	if err := s.store.Tag().Create(t); err != nil {
		return err
	}
	return nil
}

func (s *TagsService) Update(t *model.Tag) (*model.Tag, error) {

	tag, err := s.store.Tag().FindId(t.ID)
	if err != nil {
		return nil, err
	}

	if t.Name != "" {
		tag.Name = t.Name
	}
	if t.Alias != "" {
		tag.Alias = t.Alias
	}

	if err := s.store.Tag().Update(tag); err != nil {
		return nil, err
	}
	return tag, nil
}

func (s *TagsService) Delete(id string) error {
	if _, err := s.store.Tag().Delete(id); err != nil {
		return err
	}
	return nil
}

func (s *TagsService) FindId(id string) (*model.Tag, error) {

	t, err := s.store.Tag().FindId(id)
	if err != nil {
		return nil, err
	}
	return t, nil
}

func (s *TagsService) FindAll() ([]model.Tag, error) {

	tags, err := s.store.Tag().FindAll()
	if err != nil {
		return nil, err
	}
	return tags, nil
}
func (s *TagsService) FindAllPagination(cs *model.Search) ([]model.Tag, error) {

	tags, err := s.store.Tag().FindAllPagination(cs)
	if err != nil {
		return nil, err
	}
	return tags, nil
}
