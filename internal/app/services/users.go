package service

import (
	"strconv"
	"time"

	"bitbucket.org/mavz/restapi.git/internal/app/model"
	"bitbucket.org/mavz/restapi.git/internal/app/store"

	"bitbucket.org/mavz/restapi.git/pkg/auth"
)

type UsersService struct {
	store           store.Store
	tokenManager    auth.TokenManager
	accessTokenTTL  time.Duration
	refreshTokenTTL time.Duration
}

func NewUsersService(store store.Store, tokenManager auth.TokenManager, accessTokenTTL time.Duration, refreshTokenTTL time.Duration) *UsersService {
	return &UsersService{
		store:           store,
		tokenManager:    tokenManager,
		accessTokenTTL:  accessTokenTTL,
		refreshTokenTTL: refreshTokenTTL,
	}
}

func (s *UsersService) RefreshTokens(refreshToken string) (Tokens, error) {
	session, err := s.store.Session().FindByToket(refreshToken)

	if err != nil || !session.IsValid() {
		return Tokens{}, err
	}

	return s.createSession(session.User)
}

func (s *UsersService) SignIn(email string, pass string) (Tokens, error) {

	u, err := s.store.User().FindByEmail(email)
	if err != nil || !u.ComparePassword(pass) {
		return Tokens{}, err
	}
	return s.createSession(u.ID)
}

func (s *UsersService) CreateUser(user *model.User) error {

	if err := s.store.User().Create(user); err != nil {
		return err
	}
	user.Sanitize()
	return nil
}

func (s *UsersService) SignUp(userId int) error {
	if err := s.store.Session().DeleteByUser(userId); err != nil {
		return err
	}
	return nil
}

func (s *UsersService) createSession(UserId int) (Tokens, error) {
	var (
		res Tokens
		err error
	)
	res.AccessToken, err = s.tokenManager.NewJWT(strconv.Itoa(UserId), s.accessTokenTTL)
	if err != nil {
		return res, err
	}

	res.RefreshToken, err = s.tokenManager.NewRefreshToken()
	if err != nil {
		return res, err
	}
	data := time.Now().Add(s.refreshTokenTTL)
	sessionNew := &model.Session{
		User:  UserId,
		Data:  data,
		Token: res.RefreshToken,
	}
	err = s.store.Session().AddSession(sessionNew)
	return res, err
}
