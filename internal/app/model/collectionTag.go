package model

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

type CollectionTag struct {
	TagId        string `json:"tag_id"`
	CollectionId string `json:"collection_id"`
}

func (ct *CollectionTag) Validate() error {
	return validation.ValidateStruct(
		ct,
		validation.Field(&ct.TagId, is.UUID),
		validation.Field(&ct.CollectionId, is.UUID),
	)
}
