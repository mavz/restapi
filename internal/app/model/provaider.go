package model

import (
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/google/uuid"
)

type Provaider struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

func (p *Provaider) Validate() error {
	return validation.ValidateStruct(
		p,
		validation.Field(&p.Name, validation.Required),
		validation.Field(&p.ID, validation.By(requiredIf(p.ID != "")), is.UUID),
	)
}

func (p *Provaider) BeforeCreate() error {
	fmt.Println(len(p.ID))
	if len(p.ID) != 36 {
		p.ID = uuid.New().String()
	}
	return nil
}
