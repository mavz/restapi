package model

import "strconv"

type Search struct {
	Page           int
	Sort           string
	SortDirection  string
	CountItemsPage int
	CountPages     int
	CountItemsAll  int
	Qstring        string
	Fields         map[string]string
	VariantsSort   []string //доступные для сортировки поля
}

func (s *Search) AddField(k string, v string) {
	if k == "" || v == "" {
		return
	}
	s.Fields[k] = v
}

func (s *Search) SetStringPage(p string) {
	page := 1
	page, err := strconv.Atoi(p)
	if err != nil {
		return
	}
	if page == 0 {
		page = 1
	}
	s.Page = page
}

func (s *Search) SetStringCountItemsPage(p string) {
	countItemsPage := 10
	countItemsPage, err := strconv.Atoi(p)
	if err != nil {
		return
	}
	s.CountItemsPage = countItemsPage
}

//возвращает проверенное имя столбца для опредления сортировки
func (s *Search) GetStringSort() (str string) {
	if s.Sort == "" || len(s.VariantsSort) == 0 {
		return
	}
	if contains(s.VariantsSort, s.Sort) {
		return s.Sort
	}
	return
}

// Contains указывает, содержится ли x в a.
func contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}
