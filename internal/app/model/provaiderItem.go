package model

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/google/uuid"
)

type ProvaiderItem struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Alias       string `json:"alias"`
	ProvaiderId string `json:"provaider_id"`
	Tags        []Tag  `json:"tags"`
}

func (pi *ProvaiderItem) Validate() error {
	return validation.ValidateStruct(
		pi,
		validation.Field(&pi.Name, validation.Required),
		validation.Field(&pi.Alias, validation.Required),
		validation.Field(&pi.ID, validation.By(requiredIf(pi.ID != "")), is.UUID),
		validation.Field(&pi.ProvaiderId, validation.By(requiredIf(pi.ProvaiderId != "")), is.UUID),
	)
}

func (pi *ProvaiderItem) BeforeCreate() error {
	//fmt.Println(len(pi.ID))
	if len(pi.ID) != 36 {
		pi.ID = uuid.New().String()
	}
	return nil
}
