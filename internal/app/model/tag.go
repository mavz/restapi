package model

import (
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/google/uuid"
)

type Tag struct {
	ID    string `json:"id"`
	Name  string `json:"name"`
	Alias string `json:"alias"`
}

func (t *Tag) Validate() error {
	return validation.ValidateStruct(
		t,
		validation.Field(&t.Name, validation.Required),
		validation.Field(&t.Alias, validation.Required),
		validation.Field(&t.ID, validation.By(requiredIf(t.ID != "")), is.UUID),
	)
}

func (t *Tag) BeforeCreate() error {
	fmt.Println(len(t.ID))
	if len(t.ID) != 36 {
		t.ID = uuid.New().String()
	}
	return nil
}

