package model

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

type ProvaiderItemTag struct {
	TagId       string `json:"tag_id"`
	ProvaiderId string `json:"provaider_id"`
}

func (ct *ProvaiderItemTag) Validate() error {
	return validation.ValidateStruct(
		ct,
		validation.Field(&ct.TagId, is.UUID),
		validation.Field(&ct.ProvaiderId, is.UUID),
	)
}
