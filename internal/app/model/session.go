package model

import (
	"time"

	validation "github.com/go-ozzo/ozzo-validation"
)

type Session struct {
	Token string
	User  int
	Data  time.Time
}

func (s *Session) Validate() error {
	return validation.ValidateStruct(
		s,
		validation.Field(&s.Token, validation.Required),
		validation.Field(&s.User, validation.Required),
	)
}

func (s *Session) IsValid() bool {
	//tplan := s.Data.Add(30 * 24 * time.Hour)
	tnow := time.Now()
	if s.Data.After(tnow) {
		return true
	}
	return false
}
