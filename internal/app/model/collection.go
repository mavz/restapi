package model

import (
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/google/uuid"
)

type Collection struct {
	ID    string `json:"id"`
	Name  string `json:"name"`
	Alias string `json:"alias"`
}

func (c *Collection) Validate() error {
	return validation.ValidateStruct(
		c,
		validation.Field(&c.Name, validation.Required),
		validation.Field(&c.Alias, validation.Required),
		validation.Field(&c.ID, validation.By(requiredIf(c.ID != "")), is.UUID),
	)
}

func (c *Collection) BeforeCreate() error {
	fmt.Println(len(c.ID))
	if len(c.ID) != 36 {
		c.ID = uuid.New().String()
	}
	return nil
}
