package apiserver

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"time"

	"github.com/google/uuid"

	service "bitbucket.org/mavz/restapi.git/internal/app/services"
	"bitbucket.org/mavz/restapi.git/pkg/auth"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/sirupsen/logrus"
)

const (
	sessionName = "mavzTest"
	ctxKeyRequestID
)

var (
	errIncorrectEmailOrPassword = errors.New("incorrect email or password")
	errNotAuthenticated         = errors.New("not authenticated")
	errNotID                    = errors.New("not ID")
	errNotParentID              = errors.New("not parent ID")
)

//var userId int = 0

type ctxKey int8

type server struct {
	router       *mux.Router
	logger       *logrus.Logger
	sessionStore sessions.Store
	services     *service.Services
	tokenManager auth.TokenManager
	userId       int
}

func newServer(sessionStore sessions.Store, services *service.Services, tokenManager auth.TokenManager) *server {
	s := &server{
		router:       mux.NewRouter(),
		logger:       logrus.New(),
		sessionStore: sessionStore,
		tokenManager: tokenManager,
		services:     services,
	}
	s.configureRouter()
	return s
}

func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}

func (s *server) configureRouter() {

	//s.router. := cors.Default().Handler(mux)
	s.router.Use(s.setRequestId)
	s.router.Use(s.logRequest)

	private := s.router.PathPrefix("/").Subrouter()
	private.Use(s.authenticateJwtUser)
	//s.router.Use(handlers.CORSOption())
	//s.router.Use(handlers.CORS(handlers.AllowCredentials()))
	s.router.HandleFunc("/users", s.handleUsersCreate()).Methods("POST")
	//s.router.HandleFunc("/sessions", s.handleSessionsCreate()).Methods("POST")
	s.router.HandleFunc("/auth/sign-in", s.handleSignIn()).Methods("POST")
	s.router.HandleFunc("/auth/refresh-token", s.handleRefreshToken()).Methods("POST")
	s.router.HandleFunc("/auth/logout", s.handleLogout()).Methods("POST")
	//collection
	private.HandleFunc("/collection/{id}", s.handleCollectionGetId()).Methods("GET")
	private.HandleFunc("/collections/all", s.handleCollectionsAllGet()).Methods("GET")
	private.HandleFunc("/collections", s.handleCollectionsPaginationGet()).Methods("GET")
	private.HandleFunc("/collection/create", s.handleCollectionCreate()).Methods("POST")
	private.HandleFunc("/collection/update/{id}", s.handleCollectionUpdate()).Methods("PUT")
	private.HandleFunc("/collection/delete/{id}", s.handleCollectionDelete()).Methods("DELETE")
	//tag
	private.HandleFunc("/tag/{id}", s.handleTagGetId()).Methods("GET")
	private.HandleFunc("/tags/all", s.handleTagsAllGet()).Methods("GET")
	private.HandleFunc("/tags", s.handleTagsPaginationGet()).Methods("GET")
	private.HandleFunc("/tag/create", s.handleTagCreate()).Methods("POST")
	private.HandleFunc("/tag/update/{id}", s.handleTagUpdate()).Methods("PUT")
	private.HandleFunc("/tag/delete/{id}", s.handleTagDelete()).Methods("DELETE")
	//provaiderItem
	private.HandleFunc("/provaideritem/{id}", s.handleProvaiderItemGetId()).Methods("GET")
	private.HandleFunc("/provaider/{parent_id}/allitems", s.handleProvaiderItemsAllGet()).Methods("GET")
	private.HandleFunc("/provaider/{parent_id}/items", s.handleProvaiderItemsPaginationGet()).Methods("GET")
	private.HandleFunc("/provaider/{parent_id}/createitem", s.handleProvaiderItemCreate()).Methods("POST")
	private.HandleFunc("/provaideritem/update/{id}", s.handleProvaiderItemUpdate()).Methods("PUT")
	private.HandleFunc("/provaideritem/delete/{id}", s.handleProvaiderItemDelete()).Methods("DELETE")
	//provaider
	private.HandleFunc("/provaider/{id}", s.handleProvaiderGetId()).Methods("GET")
	private.HandleFunc("/provaiders/all", s.handleProvaidersAllGet()).Methods("GET")
	private.HandleFunc("/provaiders", s.handleProvaidersPaginationGet()).Methods("GET")
	private.HandleFunc("/provaider/create", s.handleProvaiderCreate()).Methods("POST")
	private.HandleFunc("/provaider/update/{id}", s.handleProvaiderUpdate()).Methods("PUT")
	private.HandleFunc("/provaider/delete/{id}", s.handleProvaiderDelete()).Methods("DELETE")
	//collectionTags
	private.HandleFunc("/collection/{collection_id}/alltags", s.handleCollectionTagsAllGet()).Methods("GET")
	private.HandleFunc("/collection/{collection_id}/tags", s.handleCollectionTagsPaginationGet()).Methods("GET")
	private.HandleFunc("/collection/{collection_id}/selecttags", s.handleCollectionTagsSelectTagsGet()).Methods("GET")
	private.HandleFunc("/tag-collection/create", s.handleTagCollectionCreate()).Methods("POST")
	private.HandleFunc("/tag/{tag_id}/collection/{collection_id}/delete", s.handleTagCollectionDelete()).Methods("DELETE")
	//provaiderTag
	private.HandleFunc("/provaideritem/{provaideritem_id}/tags", s.handleProvaiderItemsTagsPaginationGet()).Methods("GET")
	private.HandleFunc("/provaideritem/{provaideritem_id}/selecttags", s.handleProvaideritemTagsSelectTagsGet()).Methods("GET")
	private.HandleFunc("/tag-provaideritem/create", s.handleTagProvaiderItemCreate()).Methods("POST")
	private.HandleFunc("/tag/{tag_id}/provaideritem/{provaideritem_id}/delete", s.handleTagProvaideritemDelete()).Methods("DELETE")

}

func (s *server) logRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger := s.logger.WithFields(logrus.Fields{
			"remote_addr": r.RemoteAddr,
			"request_id":  r.Context().Value(ctxKeyRequestID),
		})
		logger.Infof("started %s %s", r.Method, r.RequestURI)

		start := time.Now()
		rw := &responseWriter{w, http.StatusOK}
		next.ServeHTTP(rw, r)

		logger.Infof(
			"completed in %d %s %v",
			rw.code,
			http.StatusText(rw.code),
			time.Now().Sub(start),
		)
	})
}

func (s *server) setRequestId(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := uuid.New().String()
		w.Header().Set("X-Request-ID", id)
		next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), ctxKeyRequestID, id)))
	})
}

func (s *server) error(w http.ResponseWriter, r *http.Request, code int, err error) {
	s.respond(w, r, code, map[string]string{"error": err.Error()})
}

func (s *server) respond(w http.ResponseWriter, r *http.Request, code int, data interface{}) {
	w.WriteHeader(code)
	if data != nil {
		json.NewEncoder(w).Encode(data)
	}
}
