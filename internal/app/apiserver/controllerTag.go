package apiserver

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/mavz/restapi.git/internal/app/model"
	service "bitbucket.org/mavz/restapi.git/internal/app/services"
	"github.com/gorilla/mux"
)

func (s *server) handleTagCreate() http.HandlerFunc {
	type request struct {
		ID    string `json:"id"` //не обязательный параметр
		Name  string `json:"name"`
		Alias string `json:"alias"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}
		t := &model.Tag{
			ID:    req.ID,
			Name:  req.Name,
			Alias: req.Alias,
		}
		if err := s.services.Tags.Create(t); err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusCreated, t)
	}
}

func (s *server) handleTagUpdate() http.HandlerFunc {
	type request struct {
		Name  string `json:"name"`
		Alias string `json:"alias"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}
		params := mux.Vars(r)
		id := params["id"]
		if id == "" {
			s.error(w, r, http.StatusBadRequest, errNotID)
			return
		}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}
		t := &model.Tag{
			ID:    id,
			Name:  req.Name,
			Alias: req.Alias,
		}
		tag, err := s.services.Tags.Update(t)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusCreated, tag)
	}
}

func (s *server) handleTagDelete() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		id := params["id"]

		err := s.services.Tags.Delete(id)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusOK, "")
	}
}

func (s *server) handleTagGetId() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		id := params["id"]

		t, err := s.services.Tags.FindId(id)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusOK, t)
	}
}

func (s *server) handleTagsPaginationGet() http.HandlerFunc {
	type response struct {
		Items      []model.Tag `json:"items"`
		Page       int         `json:"page"`
		CountPages int         `json:"countPages"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &service.SearchRequest{
			Page:           r.URL.Query().Get("page"),
			Sort:           r.URL.Query().Get("sort"),
			SortDirection:  r.URL.Query().Get("sortDirection"),
			CountItemsPage: r.URL.Query().Get("countItemsPage"),
			Qstring:        r.URL.Query().Get("q"),
		}

		cs := &model.Search{
			Sort:          req.Sort,
			SortDirection: req.SortDirection,
			VariantsSort:  []string{"name", "alias"},
			Qstring:       req.Qstring,
		}
		cs.SetStringPage(req.Page)
		cs.SetStringCountItemsPage(req.CountItemsPage)

		tags, err := s.services.Tags.FindAllPagination(cs)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		res := &response{
			Page:       cs.Page,
			CountPages: cs.CountPages,
			Items:      tags,
		}
		s.respond(w, r, http.StatusOK, res)
	}
}

func (s *server) handleTagsAllGet() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		tags, err := s.services.Tags.FindAll()
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusOK, tags)
	}
}
