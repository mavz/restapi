package apiserver

import (
	"log"
	"net/http"
	"time"

	"github.com/jmoiron/sqlx"

	"github.com/gorilla/handlers"
	"github.com/gorilla/sessions"

	service "bitbucket.org/mavz/restapi.git/internal/app/services"
	"bitbucket.org/mavz/restapi.git/internal/app/store/sqlstore"
	"bitbucket.org/mavz/restapi.git/pkg/auth"
	"bitbucket.org/mavz/restapi.git/pkg/rmq"
)

/**/
func handleError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}

}

/**/
func Start(config *Config) error {
	db, err := newDB(config.DatabaseURL)
	if err != nil {
		return err
	}
	defer db.Close()
	store := sqlstore.New(db)
	sessionsStore := sessions.NewCookieStore([]byte(config.SessionKey))
	tokenManager, err := auth.NewManager(config.SigningKey)
	accessTokenTTL := time.Duration(config.GetAccessTokenTTL())
	refreshTokenTTL := time.Duration(config.GetRefreshTokenTTL())
	rabbitClient, err := rmq.NewRabbitClient(config.RabbitConnectionURL)

	if err != nil {
		return err
	}

	services := service.NewServices(service.Deps{
		Store:           store,
		TokenManager:    tokenManager,
		AccessTokenTTL:  accessTokenTTL,
		RefreshTokenTTL: refreshTokenTTL,
		RabbitClient:    rabbitClient,
	})

	srv := newServer(sessionsStore, services, tokenManager)

	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Authorization"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})

	return http.ListenAndServe(config.BindAddr, handlers.CORS(originsOk, headersOk, methodsOk)(srv))
	//return http.ListenAndServe(config.BindAddr, handlers.CORS()(srv))
}

func newDB(databaseURL string) (*sqlx.DB, error) {
	db, err := sqlx.Open("pgx", databaseURL)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	return db, nil
}
