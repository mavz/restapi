package apiserver

import (
	"log"
	"strconv"
	"time"
)

// Config ...
type Config struct {
	BindAddr            string `toml:"bind_addr"`
	LogLevel            string `toml:"log_level"`
	DatabaseURL         string `toml:"database_url"`
	SessionKey          string `toml:"session_key"`
	SigningKey          string `toml:"signing-key"`
	AccessTokenTTL      string `toml:"accessTokenTTL"`  //минуты
	RefreshTokenTTL     string `toml:"refreshTokenTTL"` //дни
	RabbitConnectionURL string `toml:"rabbit_connection_URL"`
}

// NewConfig ...
func NewConfig() *Config {
	return &Config{
		BindAddr: ":8081",
		LogLevel: "debug",
	}
}

func (c *Config) GetAccessTokenTTL() time.Duration {
	t_int, err := strconv.Atoi(c.AccessTokenTTL)
	if err != nil {
		log.Fatal(err)
	}
	t := int64(t_int * 60 * 1000 * 1000 * 1000)
	return time.Duration(t)
}

func (c *Config) GetRefreshTokenTTL() time.Duration {
	t_int, err := strconv.Atoi(c.RefreshTokenTTL)
	if err != nil {
		log.Fatal(err)
	}
	t := int64((t_int * 60 * 60 * 24 * 1000 * 1000 * 1000))
	return time.Duration(t)
}
