package apiserver

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/mavz/restapi.git/internal/app/model"
	service "bitbucket.org/mavz/restapi.git/internal/app/services"
	"github.com/gorilla/mux"
)

func (s *server) handleProvaiderItemsTagsPaginationGet() http.HandlerFunc {
	type response struct {
		Items      []model.Tag `json:"items"`
		Page       int         `json:"page"`
		CountPages int         `json:"countPages"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &service.SearchRequest{
			Page:           r.URL.Query().Get("page"),
			Sort:           r.URL.Query().Get("sort"),
			SortDirection:  r.URL.Query().Get("sortDirection"),
			CountItemsPage: r.URL.Query().Get("countItemsPage"),
		}
		params := mux.Vars(r)
		provaiderItemId := params["provaideritem_id"]
		if provaiderItemId == "" {
			s.error(w, r, http.StatusBadRequest, errNotParentID)
			return
		}
		cs := &model.Search{
			Sort:          req.Sort,
			SortDirection: req.SortDirection,
			Fields:        make(map[string]string),
			VariantsSort:  []string{"name", "alias"},
		}
		cs.SetStringPage(req.Page)
		cs.SetStringCountItemsPage(req.CountItemsPage)

		tags, err := s.services.ProvaidersItemsTags.FindAllTagsPagination(provaiderItemId, cs)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		res := &response{
			Page:       cs.Page,
			CountPages: cs.CountPages,
			Items:      tags,
		}
		s.respond(w, r, http.StatusOK, res)
	}
}

func (s *server) handleProvaideritemTagsSelectTagsGet() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		q := r.URL.Query().Get("q")
		collectionId := r.URL.Query().Get("collection_id")
		params := mux.Vars(r)
		provaideritemId := params["provaideritem_id"]
		if provaideritemId == "" {
			s.error(w, r, http.StatusBadRequest, errNotParentID)
			return
		}
		tags, err := s.services.ProvaidersItemsTags.FindSelectTags(provaideritemId, collectionId, q)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusOK, tags)
	}
}

func (s *server) handleTagProvaiderItemCreate() http.HandlerFunc {
	type request struct {
		TagId           string `json:"tag_id"`
		ProvaiderItemId string `json:"provaideritem_id"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}
		pit := &model.ProvaiderItemTag{
			TagId:       req.TagId,
			ProvaiderId: req.ProvaiderItemId,
		}
		if err := s.services.ProvaidersItemsTags.AddTag(pit); err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusCreated, pit)
	}
}

func (s *server) handleTagProvaideritemDelete() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		pit := &model.ProvaiderItemTag{
			TagId:       params["tag_id"],
			ProvaiderId: params["provaideritem_id"],
		}
		err := s.services.ProvaidersItemsTags.DeleteTag(pit)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusOK, "")
	}
}
