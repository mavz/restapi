package apiserver

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/mavz/restapi.git/internal/app/model"
	service "bitbucket.org/mavz/restapi.git/internal/app/services"
	"bitbucket.org/mavz/restapi.git/pkg/logger"
	"github.com/gorilla/mux"
)

func (s *server) handleCollectionCreate() http.HandlerFunc {
	type request struct {
		ID    string `json:"id"` //не обязательный параметр
		Name  string `json:"name"`
		Alias string `json:"alias"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}
		c := &model.Collection{
			ID:    req.ID,
			Name:  req.Name,
			Alias: req.Alias,
		}
		if err := s.services.Collections.Create(c); err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusCreated, c)
	}
}

func (s *server) handleCollectionUpdate() http.HandlerFunc {
	type request struct {
		Name  string `json:"name"`
		Alias string `json:"alias"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}
		params := mux.Vars(r)
		id := params["id"]
		if id == "" {
			s.error(w, r, http.StatusBadRequest, errNotID)
			return
		}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}
		c := &model.Collection{
			ID:    id,
			Name:  req.Name,
			Alias: req.Alias,
		}
		collection, err := s.services.Collections.Update(c)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusCreated, collection)
	}
}

func (s *server) handleCollectionDelete() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		id := params["id"]

		err := s.services.Collections.Delete(id)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusOK, "")
	}
}

func (s *server) handleCollectionGetId() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		id := params["id"]

		c, err := s.services.Collections.FindId(id)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusOK, c)
	}
}

func (s *server) handleCollectionsPaginationGet() http.HandlerFunc {
	type response struct {
		Items      []model.Collection `json:"items"`
		Page       int                `json:"page"`
		CountPages int                `json:"countPages"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &service.SearchRequest{
			Page:           r.URL.Query().Get("page"),
			Sort:           r.URL.Query().Get("sort"),
			SortDirection:  r.URL.Query().Get("sortDirection"),
			CountItemsPage: r.URL.Query().Get("countItemsPage"),
			Qstring:        r.URL.Query().Get("q"),
		}

		cs := &model.Search{
			Sort:          req.Sort,
			SortDirection: req.SortDirection,
			VariantsSort:  []string{"name", "alias"},
			Qstring:       req.Qstring,
		}
		cs.SetStringPage(req.Page)
		cs.SetStringCountItemsPage(req.CountItemsPage)
		logger.Info("Пагинация: ", cs)
		//		//тестируем rabbitMq
		//s.services.TestRabbitPublisher.AddRandomInts()
		//
		collections, err := s.services.Collections.FindAllPagination(cs)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		res := &response{
			Page:       cs.Page,
			CountPages: cs.CountPages,
			Items:      collections,
		}
		s.respond(w, r, http.StatusOK, res)
	}
}

func (s *server) handleCollectionsAllGet() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		collections, err := s.services.Collections.FindAll()
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusOK, collections)
	}
}
