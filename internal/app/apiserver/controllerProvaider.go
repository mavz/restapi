package apiserver

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/mavz/restapi.git/internal/app/model"
	service "bitbucket.org/mavz/restapi.git/internal/app/services"
	"github.com/gorilla/mux"
)

func (s *server) handleProvaiderCreate() http.HandlerFunc {
	type request struct {
		ID   string `json:"id"` //не обязательный параметр
		Name string `json:"name"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}
		p := &model.Provaider{
			ID:   req.ID,
			Name: req.Name,
		}
		if err := s.services.Provaiders.Create(p); err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusCreated, p)
	}
}

func (s *server) handleProvaiderUpdate() http.HandlerFunc {
	type request struct {
		Name string `json:"name"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}
		params := mux.Vars(r)
		id := params["id"]
		if id == "" {
			s.error(w, r, http.StatusBadRequest, errNotID)
			return
		}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}
		p := &model.Provaider{
			ID:   id,
			Name: req.Name,
		}
		provaider, err := s.services.Provaiders.Update(p)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusCreated, provaider)
	}
}

func (s *server) handleProvaiderDelete() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		id := params["id"]

		err := s.services.Provaiders.Delete(id)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusOK, "")
	}
}

func (s *server) handleProvaiderGetId() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		id := params["id"]

		p, err := s.services.Provaiders.FindId(id)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusOK, p)
	}
}

func (s *server) handleProvaidersPaginationGet() http.HandlerFunc {
	type response struct {
		Items      []model.Provaider `json:"items"`
		Page       int               `json:"page"`
		CountPages int               `json:"countPages"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &service.SearchRequest{
			Page:           r.URL.Query().Get("page"),
			Sort:           r.URL.Query().Get("sort"),
			SortDirection:  r.URL.Query().Get("sortDirection"),
			CountItemsPage: r.URL.Query().Get("countItemsPage"),
			Qstring:        r.URL.Query().Get("q"),
		}

		cs := &model.Search{
			Sort:          req.Sort,
			SortDirection: req.SortDirection,
			VariantsSort:  []string{"name", "alias"},
			Qstring:       req.Qstring,
		}
		cs.SetStringPage(req.Page)
		cs.SetStringCountItemsPage(req.CountItemsPage)

		provaiders, err := s.services.Provaiders.FindAllPagination(cs)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		res := &response{
			Page:       cs.Page,
			CountPages: cs.CountPages,
			Items:      provaiders,
		}
		s.respond(w, r, http.StatusOK, res)
	}
}

func (s *server) handleProvaidersAllGet() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		provaiders, err := s.services.Provaiders.FindAll()
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusOK, provaiders)
	}
}
