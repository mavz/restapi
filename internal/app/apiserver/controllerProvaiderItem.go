package apiserver

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/mavz/restapi.git/internal/app/model"
	service "bitbucket.org/mavz/restapi.git/internal/app/services"
	"github.com/gorilla/mux"
)

func (s *server) handleProvaiderItemCreate() http.HandlerFunc {
	type request struct {
		ID    string `json:"id"` //не обязательный параметр
		Name  string `json:"name"`
		Alias string `json:"alias"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}
		params := mux.Vars(r)
		parentId := params["parent_id"]
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}
		if parentId == "" {
			s.error(w, r, http.StatusBadRequest, errNotParentID)
			return
		}
		pi := &model.ProvaiderItem{
			ID:          req.ID,
			Name:        req.Name,
			Alias:       req.Alias,
			ProvaiderId: parentId,
		}
		if err := s.services.ProvaiderItems.Create(pi); err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusCreated, pi)
	}
}

func (s *server) handleProvaiderItemUpdate() http.HandlerFunc {
	type request struct {
		Name  string `json:"name"`
		Alias string `json:"alias"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}
		params := mux.Vars(r)
		id := params["id"]
		if id == "" {
			s.error(w, r, http.StatusBadRequest, errNotID)
			return
		}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}
		pi := &model.ProvaiderItem{
			ID:    id,
			Name:  req.Name,
			Alias: req.Alias,
		}
		provaiderItem, err := s.services.ProvaiderItems.Update(pi)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusCreated, provaiderItem)
	}
}

func (s *server) handleProvaiderItemDelete() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		id := params["id"]

		err := s.services.ProvaiderItems.Delete(id)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusOK, "")
	}
}

func (s *server) handleProvaiderItemGetId() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		id := params["id"]

		pi, err := s.services.ProvaiderItems.FindId(id)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusOK, pi)
	}
}

func (s *server) handleProvaiderItemsPaginationGet() http.HandlerFunc {
	type response struct {
		Items      []model.ProvaiderItem `json:"items"`
		Page       int                   `json:"page"`
		CountPages int                   `json:"countPages"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &service.SearchRequest{
			Page:           r.URL.Query().Get("page"),
			Sort:           r.URL.Query().Get("sort"),
			SortDirection:  r.URL.Query().Get("sortDirection"),
			CountItemsPage: r.URL.Query().Get("countItemsPage"),
			Qstring:        r.URL.Query().Get("q"),
		}
		params := mux.Vars(r)
		parentId := params["parent_id"]
		if parentId == "" {
			s.error(w, r, http.StatusBadRequest, errNotParentID)
			return
		}
		cs := &model.Search{
			Sort:          req.Sort,
			SortDirection: req.SortDirection,
			VariantsSort:  []string{"name", "alias"},
			Qstring:       req.Qstring,
		}
		cs.SetStringPage(req.Page)
		cs.SetStringCountItemsPage(req.CountItemsPage)

		provaiderItems, err := s.services.ProvaiderItems.FindAllPagination(parentId, cs)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		res := &response{
			Page:       cs.Page,
			CountPages: cs.CountPages,
			Items:      provaiderItems,
		}
		s.respond(w, r, http.StatusOK, res)
	}
}

func (s *server) handleProvaiderItemsAllGet() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		parentId := params["parent_id"]
		if parentId == "" {
			s.error(w, r, http.StatusBadRequest, errNotParentID)
			return
		}
		provaiderItems, err := s.services.ProvaiderItems.FindAll(parentId)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusOK, provaiderItems)
	}
}
