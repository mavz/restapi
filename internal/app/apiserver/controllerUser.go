package apiserver

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/mavz/restapi.git/internal/app/model"
	service "bitbucket.org/mavz/restapi.git/internal/app/services"
	"bitbucket.org/mavz/restapi.git/pkg/logger"
)

const (
	ctxKeyUser ctxKey = iota
)

func (s *server) handleWhoami() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.respond(w, r, http.StatusOK, r.Context().Value(ctxKeyUser).(*model.User))
	}
}

func (s *server) handleRefreshToken() http.HandlerFunc {
	type request struct {
		RefreshToken string `json:"refresh_token"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}
		var (
			res service.Tokens
		)
		res, err := s.services.Users.RefreshTokens(req.RefreshToken)
		if err != nil {
			s.error(w, r, http.StatusUnauthorized, errNotAuthenticated)
			return
		}
		s.respond(w, r, http.StatusOK, res)
	}
}

func (s *server) handleLogout() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		if err := s.services.Users.SignUp(s.userId); err != nil {
			s.error(w, r, http.StatusInternalServerError, err)
			return
		}
		s.respond(w, r, http.StatusOK, "")
	}
}

func (s *server) authenticateJwtUser(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s.userId = 0
		header := r.Header.Get("Authorization")

		if header == "" {
			s.error(w, r, http.StatusUnauthorized, errors.New("empty auth header"))
			return
		}

		headerParts := strings.Split(header, " ")
		if len(headerParts) != 2 || headerParts[0] != "Bearer" {
			s.error(w, r, http.StatusUnauthorized, errors.New("invalid auth header"))
			return
		}

		if len(headerParts[1]) == 0 {
			s.error(w, r, http.StatusUnauthorized, errors.New("token is empty"))
			return
		}
		id, err := s.tokenManager.Parse(headerParts[1])
		if err != nil {
			s.error(w, r, http.StatusUnauthorized, err)
			return
		}
		userId, err := strconv.Atoi(id)
		if err != nil {
			s.error(w, r, http.StatusUnauthorized, err)
			return
		}
		s.userId = userId
		logger.Info("Пользователь в системе: ", userId)
		next.ServeHTTP(w, r)
	})
}

func (s *server) handleSignIn() http.HandlerFunc {
	type request struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}

		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}
		res, err := s.services.Users.SignIn(req.Email, req.Password)
		if err != nil {
			s.error(w, r, http.StatusUnauthorized, errIncorrectEmailOrPassword)
			return
		}
		s.respond(w, r, http.StatusOK, res)
	}
}

func (s *server) handleUsersCreate() http.HandlerFunc {
	type request struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}
		u := &model.User{
			Email:    req.Email,
			Password: req.Password,
		}
		if err := s.services.Users.CreateUser(u); err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		u.Sanitize()
		s.respond(w, r, http.StatusCreated, u)
	}
}
