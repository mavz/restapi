package apiserver

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/mavz/restapi.git/internal/app/model"
	service "bitbucket.org/mavz/restapi.git/internal/app/services"
	"github.com/gorilla/mux"
)

func (s *server) handleCollectionTagsAllGet() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		collectionId := params["collection_id"]
		if collectionId == "" {
			s.error(w, r, http.StatusBadRequest, errNotParentID)
			return
		}
		tags, err := s.services.CollectionsTags.FindAllTags(collectionId)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusOK, tags)
	}
}

func (s *server) handleCollectionTagsPaginationGet() http.HandlerFunc {
	type response struct {
		Items      []model.Tag `json:"items"`
		Page       int         `json:"page"`
		CountPages int         `json:"countPages"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &service.SearchRequest{
			Page:           r.URL.Query().Get("page"),
			Sort:           r.URL.Query().Get("sort"),
			SortDirection:  r.URL.Query().Get("sortDirection"),
			CountItemsPage: r.URL.Query().Get("countItemsPage"),
			Qstring:        r.URL.Query().Get("q"),
		}
		params := mux.Vars(r)
		collectionId := params["collection_id"]
		if collectionId == "" {
			s.error(w, r, http.StatusBadRequest, errNotParentID)
			return
		}
		cs := &model.Search{
			Sort:          req.Sort,
			SortDirection: req.SortDirection,
			Fields:        make(map[string]string),
			VariantsSort:  []string{"name", "alias"},
			Qstring:       req.Qstring,
		}
		cs.SetStringPage(req.Page)
		cs.SetStringCountItemsPage(req.CountItemsPage)

		tags, err := s.services.CollectionsTags.FindAllTagsPagination(collectionId, cs)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		res := &response{
			Page:       cs.Page,
			CountPages: cs.CountPages,
			Items:      tags,
		}
		s.respond(w, r, http.StatusOK, res)
	}
}

func (s *server) handleCollectionTagsSelectTagsGet() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		q := r.URL.Query().Get("q")
		params := mux.Vars(r)
		collectionId := params["collection_id"]
		if collectionId == "" {
			s.error(w, r, http.StatusBadRequest, errNotParentID)
			return
		}
		tags, err := s.services.CollectionsTags.FindSelectTags(collectionId, q)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusOK, tags)
	}
}

func (s *server) handleTagCollectionCreate() http.HandlerFunc {
	type request struct {
		TagId        string `json:"tag_id"`
		CollectionId string `json:"collection_id"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}
		ct := &model.CollectionTag{
			TagId:        req.TagId,
			CollectionId: req.CollectionId,
		}
		if err := s.services.CollectionsTags.AddTag(ct); err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusCreated, ct)
	}
}

func (s *server) handleTagCollectionDelete() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		ct := &model.CollectionTag{
			TagId:        params["tag_id"],
			CollectionId: params["collection_id"],
		}
		err := s.services.CollectionsTags.DeleteTag(ct)
		if err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		s.respond(w, r, http.StatusOK, "")
	}
}
