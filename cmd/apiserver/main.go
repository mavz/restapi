package main

import (
	"flag"
	"fmt"
	"log"

	"github.com/BurntSushi/toml"

	"bitbucket.org/mavz/restapi.git/internal/app/apiserver"
)

var (
	configPath string
)

func init() {
	flag.StringVar(&configPath, "config-path", "configs/apiserver.toml", "path to config file")
	fmt.Println("init запустился ")
}

func main() {

	config := apiserver.NewConfig()
	//fmt.Println("начало выполнения ")
	_, err := toml.DecodeFile(configPath, config)
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Println("конфиги прочитаны ", config)
	//s := apiserver.New(config)
	if err := apiserver.Start(config); err != nil {
		log.Fatal(err)
	}

}
